import React, {useState, useEffect, useCallback} from 'react';
import {fetchMembers, addNewMember, deleteMember, editMember, randomizeAndGetResult} from "./api/secretSantaApi";
import {MemberEditForm} from "./components/MemberEditForm/MemberEditForm";
import './App.css';
import {MembersList} from "./components/MembersList/MembersList";

const initialMemberState = {
  name: '',
  email: ''
}
export const App = () => {
  const [members, setMembers] = useState([]);
  const [member, setMember] = useState(initialMemberState)
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const fetchMembersHandler = async () => {
    setIsLoading(true);
    setError(null);
    const response = await fetchMembers();
    if (response.status === 200) {
      const data = response.data;
      setMembers(data);
    } else {
      setError(response.status)
    }
    setIsLoading(false);
  }

  const changeMemberHandler = (member) => {
    setMember(member);
  }

  const saveMemberHandler = async () => {
    setIsLoading(true);
    setError(null);
    if (member.id) {
      const response = await editMember(member, member.id)
      if (response.status === 200) {
        const updatedMember = response.data;
        setMembers(oldMembers => {
          const index = oldMembers.findIndex(element => element.id === updatedMember.id)
          oldMembers.splice(index, 1, updatedMember)
          return oldMembers;
        })
      } else {
        setError(response.status)
      }
    } else {
      const response = await addNewMember(member)
      if (response.status === 200) {
        const newMember = response.data;
        setMembers(oldMembers => [...oldMembers, newMember])
      } else {
        setError(response.status)
      }
    }
    setMember(initialMemberState)
    setIsLoading(false);
  }

  const editMemberHandler = (member) => {
    setMember(member)
  }

  const deleteMemberHandler = async (memberId) => {
    setIsLoading(true);
    setError(null);
    const response = await deleteMember(memberId)
    if (response.status === 200) {
      const data = response.data;
      setMembers(data);
    } else {
      setError(response.status)
    }
    setIsLoading(false);
  }

  const randomizeButtonHandler = async () => {
    setIsLoading(true);
    setError(null);
    const response = await randomizeAndGetResult();
    if (response.status === 200) {
      const data = response.data;
      setMembers(data);
    } else {
      setError(response.status)
    }
    setIsLoading(false);
  }

  useEffect(() => {
    fetchMembersHandler().then();
  }, []);


  return (
    <>
      <section>
        <MemberEditForm onSaveMember={saveMemberHandler}
                        onChangeMember={changeMemberHandler}
                        member={member}/>
      </section>
      <section>
        <button onClick={randomizeButtonHandler}>Randomize and get result</button>
      </section>
      <section>
        <MembersList members={members}
                     onEditMember={editMemberHandler}
                     onDeleteMember={deleteMemberHandler}
        />
      </section>
    </>
  );
}
