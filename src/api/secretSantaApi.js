import axios from 'axios';

const baseURL = "http://localhost:8080/api"

const apiService = axios.create({
  baseURL,
  timeout: 100000,
  headers: {
    'Content-Type': `application/json`,
    Accept: 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
});

export const fetchMembers = () => {
  return apiService.get('/members');
};

export const addNewMember = (member) => {
  return apiService.post('/members', member)
}

export const editMember = (member, memberId) => {
  return apiService.put(`/members/${memberId}`,member)
}

export const deleteMember = (memberId) => {
  return apiService.delete(`/members/${memberId}`)
}

export const randomizeAndGetResult = () => {
  return apiService.post('/members/randomize')
}