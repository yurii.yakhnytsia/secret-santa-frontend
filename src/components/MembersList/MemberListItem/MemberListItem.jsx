import React from 'react'

import classes from './MemberListItem.module.css'
import {MemberItem} from "../../MemberItem/MemberItem";
export const MemberListItem  = ({member, onDelete, onEdit}) => {
  const onEditHandler = () => {
    onEdit(member)
  }
  const onDeleteHandler = () => {
    onDelete(member.id)
  }

  return <div className={classes.memberListItem}>
    <MemberItem name={member.name} email={member.email}/>
    <div className={classes.memberEditButtons}>
      <input type={'button'} value={'edit'} onClick={onEditHandler}/>
      <input type={'button'} value={'delete'} onClick={onDeleteHandler}/>
    </div>
    {member.receiver && <>
      <span className={classes.arrowSpan}>-></span>
      <MemberItem name={member.receiver.name} email={member.receiver.email}/>
    </>
    }
  </div>
}
