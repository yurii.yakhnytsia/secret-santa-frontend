import React from "react";

import {MemberListItem} from "./MemberListItem/MemberListItem";
import classes from './MembersList.module.css'

export const MembersList = ({members, onEditMember, onDeleteMember}) => {


  return (
    <ul className={classes.membersList}>
      {members.map((member) =>
        (<li key={member.id}>
          <MemberListItem member={member}
                          onEdit={onEditMember}
                          onDelete={onDeleteMember}/>
        </li>)
      )}
    </ul>
  )
}