import React from 'react';
import classes from './MemberEditForm.module.css';

export const MemberEditForm = ({onSaveMember, member, onChangeMember}) => {

  const submitHandler = (event) => {
    event.preventDefault();
    console.log(`${member.name} : ${member.email}`)
    onSaveMember()
  }
  const cancelHandler = (event) => {
    event.preventDefault();
    onChangeMember({name: '', email: ''})
  }

  const handleNameChange = (event) => {
    const name = event.target.value;
    onChangeMember({...member, name})
  }
  const handleEmailChange = (event) => {
    const email = event.target.value;
    onChangeMember({...member, email})
  }

  return (
    <form onSubmit={submitHandler}>
      <div className={classes.control}>
        <label htmlFor='title'>Name</label>
        <input value={member.name} onChange={handleNameChange}/>
      </div>
      <div className={classes.control}>
        <label htmlFor='opening-text'>Email</label>
        <input value={member.email} type={'email'} onChange={handleEmailChange}/>
      </div>
      <button onClick={submitHandler}>Save</button>
      <button onClick={cancelHandler}>Cancel</button>
    </form>
  );


}
