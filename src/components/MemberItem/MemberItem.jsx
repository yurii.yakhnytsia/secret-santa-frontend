import React from "react";

import classes from './MemberItem.module.css';
export const MemberItem = ({name, email}) => {

  return (
    <div className={classes.member}>
      <h3>
        name: {name}
      </h3>
      <p>
        email: {email}
      </p>
    </div>
  )
}